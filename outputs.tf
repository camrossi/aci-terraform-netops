output "tenant_name" {
  value = aci_tenant.terraform_ten.name
}

output "tenant_id" {
  value = aci_tenant.terraform_ten.id
}

output "vrf_name" {
  value = aci_vrf.vrf1.name
}
 
output "vrf_id" {  
  value = aci_vrf.vrf1.id  
}

#output "bd_name" {   
#  value = aci_bridge_domain.bd1.name            
#}
 
#output "bd_id" {   
#  value = aci_bridge_domain.bd1.id            
#}

output "vpc" {
  value = "context-[${aci_vrf.vrf1.name}]-addr-[${aci_cloud_context_profile.context_profile.primary_cidr}]"
}

output "subnet" {
  value = "subnet-[${aci_cloud_subnet.cloud_apic_subnet.ip}]"
}

output "aws_region" {
  value = var.aws_region
} 

output "cloud_zone" {
  value = var.cloud_zone
}
